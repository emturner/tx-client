<!--
SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>

SPDX-License-Identifier: MIT
-->

# TX Kernel client

Ability to interact with the TX Kernel, when deployed on a Tezos Smart Rollup.
- [Deploying the TX Kernel](#deployment)
- [How to use the client](#client)
- [Next Steps](#next-steps)

<a name="deployment"/>

## Deployment

It is possible to deploy the TX Kernel to a `Smart Rollup`. To do so, slightly modify
the [Smart Optimistic Rollup Workflow](https://tezos.gitlab.io/alpha/smart_rollups.html?highlight=smart%20rollup#workflows) as follows:

### Installer kernel

The [TX Kernel](./tx-kernel.wasm) (like many kernels) is too big to fit in an L1 operation, and therefore cannot be used to originate a rollup with. To be able to run a rollup with it, we need to generate an _installer kernel_. We will use this installer to originate the rollup - and it will immediately upgrade to the tx-kernel.

First, we need to install the `smart-rollup-installer` - a tool to make installation of larger kernels easier.
We can install this tool using cargo:

```sh
# Ensure $CARGO_HOME is available in $PATH
export PATH=$PATH:$HOME/.cargo/bin

# Install the installer client
cargo install tezos_smart_rollup_installer --git https://gitlab.com/tezos/tezos
```

You can then produce an installer for the tx-kernel. This will produce both an _installer_, but also the _preimages_ of the tx-kernel. We'll place this into the rollup node _reveal directory_ later.

```sh
smart-rollup-installer get-reveal-installer \
    --upgrade-to tx-kernel.wasm \
    --output installer.hex \
    --preimages-dir wasm_2_0_0
```

### Origination

To originate a rollup with the TX Kernel, run the following:

```sh
octez-client --base-dir "${OCLIENT_DIR}" \
  originate smart rollup tx from "${OPERATOR_ADDR}" \
  of kind wasm_2_0_0 \
  of type 'pair string (ticket string)' \
  with kernel file:installer.hex \
  --burn-cap 999
```

**N.B.** we have given the smart rollup an alias of `tx`.

```sh
### Initialise the rollup config

Initialise the rollup config as normal:

```sh
octez-smart-rollup-node-alpha --base-dir "${OCLIENT_DIR}" \
                 init operator config for tx \
                 with operators "${OPERATOR_ADDR}" \
                 --data-dir "${ROLLUP_NODE_DIR}"
```

### Deploying a rollup node

*NB* Before running the rollup node, ensure that you copy the contents of [wasm_2_0_0](./wasm_2_0_0) to `${ROLLUP_NODE_DIR/wasm_2_0_0}`. The copied files correspond to the contents of the TX Kernel binary - and allows the [reveal channel](https://tezos.gitlab.io/alpha/smart_rollups.html?highlight=smart%20rollup#populating-the-reveal-channel) to find them when the installer requests:

```sh
mkdir ${ROLLUP_NODE_DIR}/wasm_2_0_0
cp ./wasm_2_0_0/* ${ROLLUP_NODE_DIR}/wasm_2_0_0
```

You can now run the rollup node:

```sh
octez-smart-rollup-node-alpha -d "${OCLIENT_DIR}" run --data-dir ${ROLLUP_NODE_DIR} \
  --log-kernel-debug --log-kernel-debug-file kernel.log
```

### Running

You should now be able to run the rollup node.

<a name="client"/>

## TX Kernel Client

A CLI client for interacting with the TX Kernel, running on a smart rollup.

- [Prerequisites](#client-prereq)
- [Client setup](#client-setup)
- [TX Kernel account keys](#rollup-keys)
- [Depositing tickets to the rollup](#deposit)
- [Checking account balances](#balance)
- [Transferring tickets](#transfer)

<a name="client-prereq"/>

### Prerequisites

First, a running smart rollup node, for a rollup with the TX Kernel deployed.

Secondly, the `mint_and_deposit_ticket.tz` contract should be deployed:

```sh
octez-client -d "${OCLIENT_DIR}" \
  originate contract mint_and_deposit_to_rollup \
  transferring 0 from <alias> running file:mint_and_deposit_to_rollup.tz \
  --burn-cap 0.1155 --init "Unit"
```

You should get something like the following output:
```sh
Warning:

                 This is NOT the Tezos Mainnet.

           Do NOT use your fundraiser keys on this network.

Node is bootstrapped.
Estimated gas: 1452.511 units (will add 100 for safety)
Estimated storage: 462 bytes added (will add 20 for safety)
Operation successfully injected in the node.
Operation hash is 'oon8t1UZFVtAheGePjTzVbtoYbbtwAJtoChScFPciDidxrnztkB'
Waiting for the operation to be included...
Operation found in block: BM9Ux7fmAAxrMkWQ5yMrtbssCKcYcZAtU7T4gNbgawWUTmJDrVf (pass: 3, offset: 0)
This sequence of operations was run:
  Manager signed operations:
    From: tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP
    Fee to the baker: ꜩ0.000588
    Expected counter: 13
    Gas limit: 1553
    Storage limit: 482 bytes
    Balance updates:
      tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP ... -ꜩ0.000588
      payload fees(the block proposer) ....... +ꜩ0.000588
    Origination:
      From: tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP
      Credit: ꜩ0
      Script:
        { parameter
            (pair (pair (contract %rollup (pair string (ticket string))) (string %rollup_account))
                  (pair (nat %ticket_amount) (string %ticket_content))) ;
          storage unit ;
          code { CAR ;
                 DUP ;
                 CAR ;
                 CAR ;
                 PUSH mutez 0 ;
                 DUP 3 ;
                 CDR ;
                 CAR ;
                 DUP 4 ;
                 CDR ;
                 CDR ;
                 TICKET ;
                 ASSERT_SOME ;
                 DIG 3 ;
                 CAR ;
                 CDR ;
                 PAIR ;
                 TRANSFER_TOKENS ;
                 PUSH unit Unit ;
                 NIL operation ;
                 DIG 2 ;
                 CONS ;
                 PAIR } }
        Initial storage: Unit
        No delegate for this contract
        This origination was successfully applied
        Originated contracts:
          KT1K4eLQWuwJ9THEpExqbCyyqPDr4RC8taF7
        Storage size: 205 bytes
        Paid storage size diff: 205 bytes
        Consumed gas: 1452.511
        Balance updates:
          tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP ... -ꜩ0.05125
          storage fees ........................... +ꜩ0.05125
          tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP ... -ꜩ0.06425
          storage fees ........................... +ꜩ0.06425

New contract KT1K4eLQWuwJ9THEpExqbCyyqPDr4RC8taF7 originated.
The operation has only been included 0 blocks ago.
We recommend to wait more.
Use command
  octez-client wait for oon8t1UZFVtAheGePjTzVbtoYbbtwAJtoChScFPciDidxrnztkB to be included --confirmations 1 --branch BLDWyJd3LsBLiK8zyQbnP972wFGZ2eVGz4YEeohjuCLsUHXXvR5
and/or an external block explorer.
Contract memorized as mint_and_deposit_to_rollup.
```

You will also need `rust-1.60` installed, if building from source - alternatively the `shell.nix` file may be used to give a CLI with all required software for the client to be built.

If building from source, the `alias tx-client='cargo run -q --'` may be useful.

<a name="client-setup"/>

### Setting up the client

Initialise the client configuration. You should pass through the commands that you use to run `octez-client` and `octez-smart-rollup-client-alpha` (full-paths to the binaries):

```sh
export TX_CONFIG='tx-client.json'

tx-client --config-file ${TX_CONFIG} \
          config-init \
          --tz-client "octez-client" \
          --tz-client-base-dir "${OCLIENT_DIR}" \
          --tz-rollup-client "octez-smart-rollup-client-alpha" \
          --forwarding-account <alias>
```

This will check your setup, by fetching the address of the `mint_and_deposit_to_rollup` contract, and the rollup address.
`forwarding-account` must be a Layer-1 (testnet) account with sufficient tez for sending messages to the rollup.

If succesful, running `cat ${TX_CONFIG} | jq` should give something like:

```json
{
  "tz_client": "/home/emma/sources/tezos/octez-client",
  "tz_client_base_dir": "/home/emma/sources/mondaynet/.tezos-client",
  "tz_rollup_client": "/home/emma/sources/tezos/octez-smart-rollup-client-alpha",
  "forwarding_account": "alice",
  "depositor_address": "KT1K4eLQWuwJ9THEpExqbCyyqPDr4RC8taF7",
  "rollup_address": "sr19ySqGBtbRaKwpC9YYa1udbfRMs4bVZDHM",
}
```


<a name="rollup-keys"/>

### TX Account keys

When depositing into the TX Kernel, you choose an address - that account is automatically created. To be able to withdraw from the account, you need to sign an external (binary) message to the kernel, using the private key corresponding to the account.

Let's create an account key with an alias:

```sh
tx-client --config-file ${TX_CONFIG} gen-key alice_in_wonderland
```

<a name="deposit"/>

### Depositing tickets to the rollup

The `tx-client` can deposit tickets into the rollup, making use of the `mint_and_deposit_to_rollup` contract as the _ticketer_. The TX Kernel deals with tickets with _string contents_.

Lets create a ticket and send it to the account we made just now:

```sh
tx-client --config-file ${TX_CONFIG} \
          mint-and-deposit \
          --amount 10 \
          --contents "The Looking Glass" \
          --target alice_in_wonderland \
          --ticket-alias mirror
```

You should see something like the following:

```
Warning:

                 This is NOT the Tezos Mainnet.

           Do NOT use your fundraiser keys on this network.

Node is bootstrapped.
Estimated gas: 3344.985 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'opSQJJT6wx2fqkwukMyVSph2qirubkyevEQMe6MjvGDKdKCdogC'
Waiting for the operation to be included...
Operation found in block: BLqpjK6uTKa35MgRJeYZMTt2B5iUZdUxcEWwjabkcp5UAi5atdy (pass: 3, offset: 0)
This sequence of operations was run:
  Manager signed operations:
    From: tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP
    Fee to the baker: ꜩ0.000714
    Expected counter: 21
    Gas limit: 3445
    Storage limit: 0 bytes
    Balance updates:
      tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP ... -ꜩ0.000714
      payload fees(the block proposer) ....... +ꜩ0.000714
    Transaction:
      Amount: ꜩ0
      From: tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP
      To: KT1K4eLQWuwJ9THEpExqbCyyqPDr4RC8taF7
      Parameter: (Pair (Pair "sr19ySqGBtbRaKwpC9YYa1udbfRMs4bVZDHM"
                             "0cbb35d37f2760d341c2bc61d619da8b8b84b230")
                       (Pair 10 "The Looking Glass"))
      This transaction was successfully applied
      Updated storage: Unit
      Storage size: 205 bytes
      Consumed gas: 2338.812
      Internal operations:
        Internal Transaction:
          Amount: ꜩ0
          From: KT1K4eLQWuwJ9THEpExqbCyyqPDr4RC8taF7
          To: sr19ySqGBtbRaKwpC9YYa1udbfRMs4bVZDHM
          Parameter: (Pair "0cbb35d37f2760d341c2bc61d619da8b8b84b230"
                           (Pair 0x0172f6de83625d1d2a57c9db6c3dde3b7f479cb53200 (Pair "The Looking Glass" 10)))
          This transaction was successfully applied
          Consumed gas: 1006.207
          Ticket updates:
            Ticketer: KT1K4eLQWuwJ9THEpExqbCyyqPDr4RC8taF7
            Content type: string
            Content: "The Looking Glass"
            Account updates:
              sr19ySqGBtbRaKwpC9YYa1udbfRMs4bVZDHM ... +10

The operation has only been included 0 blocks ago.
We recommend to wait more.
Use command
  octez-client wait for opSQJJT6wx2fqkwukMyVSph2qirubkyevEQMe6MjvGDKdKCdogC to be included --confirmations 1 --branch BLepujeuGR8pnwBxeK9ZLmpjAfmiKY3tezuSxv9dPeVyojqn6PT
and/or an external block explorer.
```

<a name="balance"/>

### Checking account balances in the rollup

Now that we've deposited a ticket to the rollup, you can check the balance, using the aliases defined
previously:

```sh
tx-client --config-file ${TX_CONFIG} \
          get-balance \
          --account alice_in_wonderland \
          --ticket mirror
```

You should see the following:
```
alice_in_wonderland holds 10 of mirror
```

After doing further deposits, you can then check the balance again, and should see it adjust accordingly.

<a name="transfer"/>

### Transferring between accounts

To transfer to another account we send an _external_ (ie binary) message to the kernel.

First, we need another account key: 

```sh
tx-client --config-file ${TX_CONFIG} gen-key red_queen
```

This account doesn't exist yet on the rollup. Checking the balance:

```sh
tx-client --config-file ${TX_CONFIG} \
          get-balance \
          --account red_queen \
          --ticket mirror
```

Yields the following output:
```
red_queen holds 0 of mirror
```

Rather than depositing to `red_queen` directly, let's transfer some of `mirror` from `alice_in_wonderland`:
```sh
tx-client --config-file ${TX_CONFIG} \
          transfer \
          --from alice_in_wonderland \
          --to red_queen \
          --ticket mirror \
          --amount 1
```

You should then see the following output (or similar):
```
Could not fetch source account counter, defaulting to 0
Public key not linked to account, will link in transfer.
Warning:

                 This is NOT the Tezos Mainnet.

           Do NOT use your fundraiser keys on this network.

Node is bootstrapped.
Estimated gas: 1001.811 units (will add 100 for safety)
Estimated storage: no bytes added
Operation successfully injected in the node.
Operation hash is 'opHhhLMND2EkvUUmFfgCg2sPYrHPbifUsYQ9ABZh2x9tW8hFUNh'
Waiting for the operation to be included...
Operation found in block: BKmfybHL9e79aY4oA6WYKqLpWjbyQoSBo9LEoqxQYEmrqnbX6DH (pass: 3, offset: 0)
This sequence of operations was run:
  Manager signed operations:
    From: tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP
    Fee to the baker: ꜩ0.000537
    Expected counter: 27
    Gas limit: 1102
    Storage limit: 0 bytes
    Balance updates:
      tz1Z49wRNujmWndriMhoUhzRbWda9N1JQTRP ... -ꜩ0.000537
      payload fees(the block proposer) ....... +ꜩ0.000537
    Smart contract rollup messages submission
      This smart contract rollup messages submission was successfully applied
      Consumed gas: 1001.845

The operation has only been included 0 blocks ago.
We recommend to wait more.
Use command
  octez-client wait for opHhhLMND2EkvUUmFfgCg2sPYrHPbifUsYQ9ABZh2x9tW8hFUNh to be included --confirmations 1 --branch BKkTg8Wca1HoQgjG7XV6ZoSm4ta7SYrscYkzNCZa6dinE5TULiF
and/or an external block explorer.
```

If you [check the balances](#balance) for `red_queen` and `alice_in_wonderland`, you should find that the balances have changed.

*NB* What do the first two lines of output mean? Each operation out of an account increments the operation counter - but the very first time it doesn't exist yet! Similarly, the first operation out of an account has to include the account's public key. If you send another transfer, those two lines will no longer be printed - as the `tx-client` is able to retrieve the counter, and see that the public key is linked.

If you transfer back to `alice_in_wonderland` from the `red_queen` for the first time though, then those two lines will come back.

<a name="next-steps"/>

## Next steps

That was a lot, but you can now run transactions inside of Smart Rollups! Here are some more ideas to play around with:
- deposit more of the same ticket/different tickets
- transfer between more accounts
- transfer away from an account that doesn't have sufficient balance - what happens?

In the meantime, this guide, and the `tx-client` will be expanded in future:
- *withdrawals*: we went through the looking glass, but getting out is a bit trickier
- *transfers to other addresses*: currently, we can transfer between accounts that we own - what if we want to send 
  a ticket to an account we don't?
- *importing tickets*: to be able to transfer a ticket, we need to know its ticketer & contents. It should be possible
  to tell the `tx-client` about such a ticket directly.
- *better error reporting*: when a transfer fails due to sufficient balance, the `tx-client` could actually warn
  the user directly, instead of them having to manually check the balances each time.
- *Data Availability Committee integration*: the TX Kernel has support for not only doing computation off chain, but also
  sending it messages where the _operation data_ is off chain too (but still [refutable](https://tezos.gitlab.io/alpha/smart_rollups.html?highlight=smart%20rollup#refutation)).

More ideas welcome/something doesn't work: Please raise an issue on this repo, or in #scoru-ecosystem. Thank you!
