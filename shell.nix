# SPDX-FileCopyrightText: 2022 TriliTech <contact@trili.tech>
#
# SPDX-License-Identifier: MIT

let
  rust-overlay = builtins.fetchTarball "https://github.com/oxalica/rust-overlay/archive/master.tar.gz";
  pkgs =
    import
      (fetchTarball "https://github.com/NixOS/nixpkgs/archive/ed8347c8841fcfbe2002638eae5305ac8fcd2316.tar.gz")
      { overlays = [ (import rust-overlay) ]; };
in
  pkgs.mkShell {
    buildInputs = with pkgs; [
      rust-bin.stable."1.67.0".default
      rust-analyzer
      cargo-make
      clang
      wabt
    ];

    RUST_BACKTRACE = 1;

    shellHook = "export CC=clang";
  }
