// SPDX-FileCopyrightText: 2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

use serde::{Deserialize, Serialize};
use std::process::Command;

// TODO: impl Display for Path
// TODO: simulation host
#[derive(Debug, Deserialize, Serialize)]
struct DurableValue(Option<String>);

pub fn get_value(rollup_client: &str, path: &str) -> Option<Vec<u8>> {
    let path = format!("/global/block/head/durable/wasm_2_0_0/value?key={}", path);

    let output = Command::new(rollup_client)
        .args(["rpc", "get", &path])
        .output()
        .expect("Unable to run tz_rollup_client");

    if !output.status.success() {
        eprintln!("{}", String::from_utf8_lossy(&output.stderr));
        None
    } else {
        serde_json::from_slice::<DurableValue>(&output.stdout)
            .expect("Expected JSON value from durable RPC.")
            .0
            .map(hex::decode)
            .map(|v| v.expect("Expected Hex-encoded value"))
    }
}

#[allow(dead_code)]
pub fn get_subkeys(rollup_client: &str, path: &str) -> Vec<String> {
    let path = format!("/global/block/head/durable/wasm_2_0_0/subkeys?key={}", path);

    let output = Command::new(rollup_client)
        .args(["rpc", "get", &path])
        .output()
        .expect("Unable to run tz_rollup_client");

    if !output.status.success() {
        eprintln!("{}", String::from_utf8_lossy(&output.stderr));
        vec![]
    } else {
        let output = String::from_utf8_lossy(&output.stdout);
        serde_json::de::from_str(&output).unwrap()
    }
}

#[allow(dead_code)]
pub fn value_size(rollup_client: &str, path: &str) -> Option<i64> {
    let path = format!("/global/block/head/durable/wasm_2_0_0/length?key={}", path);

    let output = Command::new(rollup_client)
        .args(["rpc", "get", &path])
        .output()
        .expect("Unable to run tz_rollup_client");

    if !output.status.success() {
        eprintln!("{}", String::from_utf8_lossy(&output.stderr));
        None
    } else {
        let output = String::from_utf8_lossy(&output.stdout);

        Some(str::parse(&output).unwrap())
    }
}
