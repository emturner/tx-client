// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

use super::Account;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::io;
use std::process::Command;
use tezos_crypto_rs::hash::{ContractKt1Hash, SmartRollupHash};
use tezos_smart_rollup::michelson::ticket::TicketHash;
use thiserror::Error;

#[derive(Serialize, Deserialize, Default)]
pub(crate) struct Config {
    pub(crate) tz_client: String,
    pub(crate) tz_client_base_dir: String,
    pub(crate) tz_rollup_client: String,
    pub(crate) forwarding_account: String,
    pub(crate) depositor_address: Option<ContractKt1Hash>,
    pub(crate) rollup_address: Option<SmartRollupHash>,
    #[serde(default)]
    known_accounts: Vec<Account>,
    #[serde(default)]
    known_tickets: HashMap<String, KnownTicket>,
}

#[derive(Debug, Serialize, Deserialize)]
pub(crate) struct KnownTicket {
    pub(crate) minter: ContractKt1Hash,
    pub(crate) content: String,
    pub(crate) hash: String,
}

#[derive(Debug, Error)]
pub(crate) enum ConfigError {
    #[error("Unable to execute tz_client {0}")]
    TzClientExecuteError(std::io::Error),
    #[error("tz_client failed:\n{0}")]
    TzClientFailure(String),
    #[error("Unable to execute tz_rollup_client {0}")]
    RollupClientExecuteError(std::io::Error),
    #[error("tz_rollup_client failed:\n{0}")]
    RollupClientFailure(String),
}

impl Config {
    pub(crate) fn init_from(
        tz_client: String,
        tz_client_base_dir: String,
        tz_rollup_client: String,
        forwarding_account: String,
    ) -> Self {
        Self {
            tz_client,
            tz_rollup_client,
            tz_client_base_dir,
            forwarding_account,
            ..Self::default()
        }
    }

    pub(crate) fn save(&self, config_file: &str) -> io::Result<()> {
        let mut file = std::fs::File::create(config_file)?;

        serde_json::ser::to_writer(&mut file, self).expect("failed to save config");
        Ok(())
    }

    pub(crate) fn fetch_depositor_address(&mut self) -> Result<(), ConfigError> {
        let output = Command::new(&self.tz_client)
            .args(["--base-dir", &self.tz_client_base_dir])
            .args(["show", "known", "contract", "mint_and_deposit_to_rollup"])
            .output()
            .map_err(ConfigError::TzClientExecuteError)?;

        if !output.status.success() {
            return Err(ConfigError::TzClientFailure(
                String::from_utf8_lossy(&output.stderr).to_string(),
            ));
        }

        let address = String::from_utf8_lossy(&output.stdout)
            .split('\n')
            .filter(|s| s.starts_with("KT1"))
            .map(|s| ContractKt1Hash::from_base58_check(s).unwrap())
            .next()
            .expect("Expected KT1 Hash");

        self.depositor_address = Some(address);

        Ok(())
    }

    pub(crate) fn fetch_rollup_address(&mut self) -> Result<(), ConfigError> {
        let output = Command::new(&self.tz_rollup_client)
            .args(["get", "smart", "rollup", "address"])
            .output()
            .map_err(ConfigError::RollupClientExecuteError)?;

        if !output.status.success() {
            return Err(ConfigError::RollupClientFailure(
                String::from_utf8_lossy(&output.stderr).to_string(),
            ));
        }

        let address = String::from_utf8_lossy(&output.stdout)
            .split('\n')
            .find(|s| s.starts_with("sr1"))
            .map(|s| SmartRollupHash::from_base58_check(s).unwrap())
            .expect("Expected Smart Rollup Hash");

        self.rollup_address = Some(address);

        Ok(())
    }

    pub(crate) fn load(config_file: &str) -> io::Result<Self> {
        let file = std::fs::File::open(config_file)?;

        Ok(serde_json::de::from_reader(file).expect("could not load config"))
    }

    pub(crate) fn add_known_account(&mut self, account: Account) {
        if let Some(a) = self
            .known_accounts
            .iter()
            .find(|Account { name, ikm, .. }| name == &account.name || ikm == &account.ikm)
        {
            if account.name == a.name {
                eprintln!("Account name {} taken", a.name);
            } else {
                eprintln!("Account already known with name {}", a.name);
            }
        } else {
            self.known_accounts.push(account);
        }
    }

    pub(crate) fn lookup_account(&self, alias: &str) -> Option<&Account> {
        self.known_accounts
            .iter()
            .find(|Account { name, .. }| name == alias)
    }

    pub(crate) fn lookup_account_mut(&mut self, alias: &str) -> Option<&mut Account> {
        self.known_accounts
            .iter_mut()
            .find(|Account { name, .. }| name == alias)
    }

    pub(crate) fn add_known_ticket(
        &mut self,
        minter: ContractKt1Hash,
        content: String,
        hash: TicketHash,
        alias: String,
    ) {
        self.known_tickets.insert(
            alias,
            KnownTicket {
                minter,
                content,
                hash: hash.to_string(),
            },
        );
    }

    pub(crate) fn lookup_ticket(&self, alias: &str) -> Option<&KnownTicket> {
        self.known_tickets.get(alias)
    }
}
