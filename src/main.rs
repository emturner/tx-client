// SPDX-FileCopyrightText: 2022-2023 TriliTech <contact@trili.tech>
//
// SPDX-License-Identifier: MIT

use clap::{Parser, Subcommand};
use config::KnownTicket;
use kernel_core::inbox::external::v1::sendable::Batch;
use kernel_core::inbox::external::v1::{Operation, OperationContent};
use kernel_core::inbox::sendable::ExternalInboxMessage;
use kernel_core::inbox::Signer;
use rand::thread_rng;
use rand::RngCore;
use serde::{Deserialize, Serialize};
use std::io;
use std::process::Command;
use tezos_crypto_rs::hash::{ContractKt1Hash, ContractTz1Hash};
use tezos_crypto_rs::hash::{PublicKeyEd25519, SecretKeyEd25519, SeedEd25519};
use tezos_crypto_rs::PublicKeyWithHash;
use tezos_data_encoding::enc::BinWriter;
use tezos_smart_rollup::inbox::ExternalMessageFrame;
use tezos_smart_rollup::michelson::ticket::StringTicket;
use tezos_smart_rollup::types::Contract;
use tezos_smart_rollup::types::SmartRollupAddress;

mod config;
mod durable;
use config::Config;

#[derive(Parser)]
#[command(long_about = None)]
struct Cli {
    #[arg(short, long, value_name = "CONFIG_FILE")]
    config_file: String,

    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    ConfigInit {
        #[arg(short = None, long, value_name = "OCTEZ_CLIENT_COMMAND")]
        tz_client: String,

        #[arg(short = None, long, value_name = "OCLIENT_DIR")]
        tz_client_base_dir: String,

        #[arg(short = None, long, value_name = "OCTEZ_ROLLUP_CLIENT_COMMAND")]
        tz_rollup_client: String,

        #[arg(short = None, long, value_name = "FORWARDING_ACCOUNT")]
        forwarding_account: String,
    },
    GenKey {
        /// Alias for account.
        account_name: String,
    },
    MintAndDeposit {
        #[arg(short, long)]
        amount: u64,
        #[arg(short, long)]
        contents: String,
        #[arg(short, long)]
        target: String,
        #[arg(short = None, long)]
        ticket_alias: String,
    },
    GetBalance {
        #[arg(short, long)]
        account: String,
        #[arg(short, long)]
        ticket: String,
        #[arg(short, long)]
        l1: bool,
    },
    Transfer {
        #[arg(short = None, long)]
        from: String,
        #[arg(short = None, long)]
        to: String,
        #[arg(short = None, long)]
        ticket: String,
        #[arg(short = None, long)]
        amount: u64,
    },
    Withdraw {
        #[arg(short = None, long)]
        from: String,
        #[arg(short = None, long)]
        ticket: String,
        #[arg(short = None, long)]
        amount: u64,
    },
    ImportTicket {
        #[arg(short, long)]
        ticketer: String,
        #[arg(short, long)]
        contents: String,
        #[arg(short, long)]
        alias: String,
    },
    ImportAddress {
        #[arg(short = None, long)]
        address: String,
        #[arg(short = None, long)]
        alias: String,
    },
}

fn main() -> io::Result<()> {
    let cli = Cli::parse();

    let config_file = cli.config_file.to_string();

    match cli.command {
        Commands::ConfigInit {
            tz_client,
            tz_client_base_dir,
            tz_rollup_client,
            forwarding_account,
        } => {
            let mut config = Config::init_from(
                tz_client,
                tz_client_base_dir,
                tz_rollup_client,
                forwarding_account,
            );

            config
                .fetch_depositor_address()
                .expect("Could not retrieve ticketer address");

            config
                .fetch_rollup_address()
                .expect("Could not retrieve rollup address");

            config.save(&config_file)?;
        }
        Commands::GenKey { account_name } => {
            let mut config = Config::load(&config_file)?;
            let account = Account::generate(account_name);

            config.add_known_account(account);

            config.save(&config_file)?;
        }
        Commands::MintAndDeposit {
            amount,
            contents,
            target,
            ticket_alias,
        } => {
            let mut config = Config::load(&config_file)?;

            deposit_ticket(&mut config, target, contents, amount, ticket_alias);

            config.save(&config_file)?;
        }
        Commands::GetBalance {
            account,
            ticket,
            l1,
        } => {
            let config = Config::load(&config_file)?;

            if !l1 {
                let balance = check_balance(&config, &account, &ticket);
                println!("{} holds {} of {}", account, balance, ticket);
            } else {
                let balance = check_l1_balance(&config, &account, &ticket);
                println!("{} holds {} of {} on l1", account, balance, ticket);
            }
        }
        Commands::Transfer {
            from,
            to,
            ticket,
            amount,
        } => {
            let config = Config::load(&config_file)?;

            transfer(&config, from, to, ticket, amount);
        }
        Commands::Withdraw {
            from,
            ticket,
            amount,
        } => {
            let mut config = Config::load(&config_file)?;

            withdraw(&mut config, from, ticket, amount);
            config.save(&config_file)?;
        }
        Commands::ImportTicket {
            ticketer,
            contents,
            alias,
        } => {
            let mut config = Config::load(&config_file)?;

            import_ticket(&mut config, ticketer, contents, alias);

            config.save(&config_file)?;
        }
        Commands::ImportAddress { address, alias } => {
            let mut config = Config::load(&config_file)?;

            let address = ContractTz1Hash::from_base58_check(&address).unwrap();

            config.add_known_account(Account {
                ikm: None,
                address,
                name: alias,
                withdrawal: None,
            });

            config.save(&config_file)?;
        }
    }

    Ok(())
}

// --------
// Accounts
// --------
#[derive(Serialize, Deserialize, Clone)]
struct Account {
    ikm: Option<String>,
    address: ContractTz1Hash,
    name: String,
    withdrawal: Option<ContractKt1Hash>,
}

impl Account {
    fn generate(name: String) -> Self {
        let mut rng = thread_rng();

        let mut ikm = vec![0; 32];
        rng.fill_bytes(ikm.as_mut_slice());

        let key = SeedEd25519(ikm.clone());
        let (pk, _) = key.keypair().unwrap();

        Self {
            ikm: Some(hex::encode(&ikm)),
            address: pk.pk_hash().unwrap(),
            name,
            withdrawal: None,
        }
    }

    fn keypair(&self) -> (PublicKeyEd25519, SecretKeyEd25519) {
        let mut ikm = vec![0; 32];
        hex::decode_to_slice(&self.ikm.as_ref().unwrap(), ikm.as_mut_slice()).unwrap();
        SeedEd25519(ikm).keypair().unwrap()
    }
}

fn import_ticket(config: &mut Config, ticketer: String, contents: String, ticket_alias: String) {
    let ticketer = ContractKt1Hash::from_base58_check(&ticketer).expect("Expected KT1 address");
    let contract = Contract::Originated(ticketer.clone());
    let ticket = StringTicket::new(contract, contents.clone(), 0);

    let ticket_hash = ticket.unwrap().hash().unwrap();

    println!("Ticket imported.");
    println!("\tidentity {}", ticket_hash);
    println!("\tticketer {}", ticketer);
    println!("\tcontents {}", contents);

    config.add_known_ticket(ticketer, contents, ticket_hash, ticket_alias);
}

fn deposit_ticket(
    config: &mut Config,
    target: String,
    contents: String,
    amount: u64,
    ticket_alias: String,
) {
    let ticketer = config.depositor_address.as_ref().unwrap().clone();
    let contract = Contract::Originated(ticketer.clone());

    let ticket_id = StringTicket::new(contract, contents.clone(), 1)
        .unwrap()
        .hash()
        .unwrap();

    config.add_known_ticket(ticketer, contents.clone(), ticket_id, ticket_alias);

    let arg = format!(
        r#"Pair (Pair "{}" "{}") (Pair {} "{}")"#,
        config.rollup_address.as_ref().unwrap(),
        config.lookup_account(&target).as_ref().unwrap().address,
        amount,
        contents
    );

    let _status = Command::new(&config.tz_client)
        .args(["--base-dir", &config.tz_client_base_dir])
        .args([
            "transfer",
            "0",
            "from",
            &config.forwarding_account,
            "to",
            &config.depositor_address.as_ref().unwrap().to_base58_check(),
            "--arg",
            &arg,
            "--burn-cap",
            "1",
        ])
        .status()
        .expect("Unable to run tz_client");
}

fn ticket_index(config: &Config, ticket_alias: &str) -> u64 {
    let ticket_index_path = format!(
        "/tickets/{}/id",
        config
            .lookup_ticket(ticket_alias)
            .as_ref()
            .expect("Unknown ticket")
            .hash
    );

    durable::get_value(&config.tz_rollup_client, &ticket_index_path)
        .unwrap()
        .try_into()
        .map(u64::from_le_bytes)
        .unwrap()
}

fn check_l1_balance(config: &Config, account: &str, ticket_alias: &str) -> u64 {
    let account = config.lookup_account(account).unwrap();
    let ticket = config.lookup_ticket(ticket_alias).unwrap();

    let output = Command::new(&config.tz_client)
        .args(["--base-dir", &config.tz_client_base_dir])
        .args([
            "get",
            "ticket",
            "balance",
            "for",
            &format!("{}", &account.address),
            "with",
            "ticketer",
            &format!("{}", &ticket.minter),
            "and",
            "type",
            "string",
            "and",
            "content",
            &format!("\"{}\"", &ticket.content),
        ])
        .output()
        .expect("Unable to run tz_client");

    let s = String::from_utf8_lossy(&output.stdout);
    s.trim().parse().unwrap()
}

fn check_balance(config: &Config, account: &str, ticket_alias: &str) -> u64 {
    let path = format!(
        "/accounts/{}/{}",
        config
            .lookup_account(account)
            .as_ref()
            .expect("Unknown account")
            .address,
        ticket_index(config, ticket_alias)
    );

    durable::get_value(&config.tz_rollup_client, &path)
        .unwrap_or_default()
        .try_into()
        .map(u64::from_le_bytes)
        .unwrap_or_default()
}

fn get_account_counter(config: &Config, address: &ContractTz1Hash) -> i64 {
    let path = format!("/accounts/{}/counter", address);

    durable::get_value(&config.tz_rollup_client, &path)
        .unwrap_or_default()
        .try_into()
        .map(i64::from_le_bytes)
        .unwrap_or_default()
}

fn is_account_pk_linked(config: &Config, address: &ContractTz1Hash) -> bool {
    let path = format!("/accounts/{}/public_key", address);

    durable::get_value(&config.tz_rollup_client, &path).is_some()
}

fn transfer(config: &Config, from: String, to: String, ticket: String, amount: u64) {
    let from_account = config
        .lookup_account(&from)
        .cloned()
        .expect("Unknown source account");
    let to_address = &config
        .lookup_account(&to)
        .as_ref()
        .expect("Unknown dest account")
        .address;
    let KnownTicket {
        minter, content, ..
    } = &config
        .lookup_ticket(&ticket)
        .as_ref()
        .expect("Unknown ticket alias");

    let ticket = StringTicket::new(
        Contract::Originated(minter.clone()),
        content.clone(),
        amount,
    )
    .unwrap();
    let ticket_hash = ticket.hash().unwrap();

    let counter = get_account_counter(config, &from_account.address);
    let (pk, sk) = from_account.keypair();

    let signer = if is_account_pk_linked(config, &from_account.address) {
        Signer::Tz1(from_account.address)
    } else {
        Signer::PublicKey(pk)
    };

    let contents = OperationContent::transfer(to_address.clone(), ticket_hash, amount).unwrap();
    let op_list = Batch::new(vec![(
        Operation {
            signer,
            counter,
            contents,
        },
        sk,
    )]);
    let message = ExternalInboxMessage::OpList(op_list);

    let mut message_bin = Vec::new();
    message.bin_write(&mut message_bin).unwrap();

    let mut message = Vec::new();
    ExternalMessageFrame::Targetted {
        address: SmartRollupAddress::new(config.rollup_address.as_ref().unwrap().clone()),
        contents: message_bin,
    }
    .bin_write(&mut message)
    .unwrap();

    let message_hex = hex::encode(message);

    let _status = Command::new(&config.tz_client)
        .args(["--base-dir", &config.tz_client_base_dir])
        .args([
            "send",
            "smart",
            "rollup",
            "message",
            &format!(r#"hex:["{}"]"#, message_hex),
            "from",
            &config.forwarding_account,
            "--burn-cap",
            "1",
        ])
        .status()
        .expect("Unable to run tz_client");
}

const WITHDRAWAL: &str = include_str!("../withdraw/withdraw.tz");

fn withdraw(config: &mut Config, from: String, ticket: String, amount: u64) {
    let tz_client = config.tz_client.clone();
    let tz_base = config.tz_client_base_dir.clone();
    let forward = config.forwarding_account.clone();

    let from_account = config
        .lookup_account_mut(&from)
        .expect("Unknown source account");

    let withdrawal = if let Some(withdrawal) = from_account.withdrawal.clone() {
        withdrawal
    } else {
        println!("Originating withdrawal contract for {}", from_account.name);

        // Originate withdrawal contract
        let output = Command::new(&tz_client)
            .args(["--base-dir", &tz_base])
            .args([
                "originate",
                "contract",
                &format!("withdraw_{}", from_account.name),
                "transferring",
                "0",
                "from",
                &forward,
                "running",
                WITHDRAWAL,
                "--init",
                &format!("\"{}\"", &from_account.address),
                "--burn-cap",
                "0.11",
            ])
            .output()
            .unwrap();

        if !output.status.success() {
            panic!(
                "Failed to initialise withdrawal contract: {}",
                String::from_utf8_lossy(&output.stderr)
            );
        }

        println!("Output: {}", String::from_utf8_lossy(&output.stdout));
        let address = String::from_utf8_lossy(&output.stdout)
            .split('\n')
            .filter(|s| s.contains("KT1"))
            .map(|s| ContractKt1Hash::from_base58_check(s.trim()).unwrap())
            .next()
            .expect("Expected KT1 Hash");

        from_account.withdrawal = Some(address.clone());

        address
    };

    let from_account = config.lookup_account(&from).unwrap();

    let KnownTicket {
        minter, content, ..
    } = &config.lookup_ticket(&ticket).expect("Unknown ticket alias");

    let ticket = StringTicket::new(
        Contract::Originated(minter.clone()),
        content.clone(),
        amount,
    )
    .unwrap();

    let counter = get_account_counter(config, &from_account.address);
    let (pk, sk) = from_account.keypair();

    let signer = if is_account_pk_linked(config, &from_account.address) {
        Signer::Tz1(from_account.address.clone())
    } else {
        Signer::PublicKey(pk)
    };

    let entrypoint = tezos_smart_rollup::types::Entrypoint::default();

    let contents = OperationContent::withdrawal(withdrawal, ticket, entrypoint);
    let op_with_key = (
        Operation {
            signer,
            counter,
            contents,
        },
        sk,
    );

    let mut message_bin = Vec::new();
    ExternalInboxMessage::OpList(Batch::new(vec![op_with_key]))
        .bin_write(&mut message_bin)
        .unwrap();

    let mut message = Vec::new();
    ExternalMessageFrame::Targetted {
        address: SmartRollupAddress::new(config.rollup_address.as_ref().unwrap().clone()),
        contents: message_bin,
    }
    .bin_write(&mut message)
    .unwrap();

    let message_hex = hex::encode(message);

    let _status = Command::new(&config.tz_client)
        .args(["--base-dir", &config.tz_client_base_dir])
        .args([
            "send",
            "smart",
            "rollup",
            "message",
            &format!(r#"hex:["{}"]"#, message_hex),
            "from",
            &config.forwarding_account,
            "--burn-cap",
            "1",
        ])
        .status()
        .expect("Unable to run tz_client");
}
