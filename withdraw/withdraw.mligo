type storage = address

type parameter = string ticket

type return = operation list * storage

let main (ticket, address : parameter * storage) : return =
  let contract = Tezos.get_contract address in
  [Tezos.transaction ticket 0tez contract], address
